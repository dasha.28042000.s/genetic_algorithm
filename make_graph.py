class Vertex:
    def __init__(self, data):
        number_, resource_size_, children_ = int(data[0]), int(data[1]), data[2:]
        self.number = number_
        self.resource_size = resource_size_
        self.children = set()
        for i in children_:
            self.children.add(int(i))
        self.parents = set()

    def get_children(self):
        return self.children

    def get_parents(self):
        return self.parents

    def get_resource(self):
        return self.resource_size

    def add_parent(self, number_of_vertex):
        self.parents.add(number_of_vertex)

    def change_parents(self, new_set_parents):
        self.parents = new_set_parents


def create_graph(file_path):
    with open(file_path, "r") as f:
        line = f.readlines()
        graph = {int(line[i].split()[0]): Vertex(line[i].split()) for i in range(1, len(line))}
    for cur_vertex in graph:
        for child in graph[cur_vertex].get_children():
            graph[child].add_parent(cur_vertex)

    set_for_work = set(i for i in graph if len(graph[i].get_parents()) == 0)
    set_end_work = set()
    set_of_vertex = set(i for i in graph)
    while set_end_work != set_of_vertex:
        tmp_set = set()
        for vertex_in_work in set_for_work:
            set_end_work.add(vertex_in_work)
            for child in graph[vertex_in_work].get_children():
                tmp_set.add(child)
                graph[child].change_parents(graph[child].get_parents() | graph[vertex_in_work].get_parents())
        set_for_work = tmp_set
    return graph


#graph = create_graph("graphs_files/dag0.txt")
