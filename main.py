import os
from deap import base, creator, tools
import random
from make_graph import create_graph
from collections import defaultdict
from itertools import cycle
import time
import matplotlib.pyplot as plt
from deap.algorithms import eaSimple


POPULATION_SIZE = 100
MIGRATION_SIZE = int(POPULATION_SIZE * 0.25)
P_CROSSOVER = 0.9
P_MUTATION = 0.1
MAX_GENERATIONS = 100
NO_CHANGE_GEN = 100
MUT_NON_STANDART = 15
NEW_P_MUTATION = 1.0


def evaluate(individual):
    pick_of_memory = 0
    state_not_ending = defaultdict(set)
    for i in individual:
        memory_cur = graph[i].get_resource()
        for j in state_not_ending:
            if state_not_ending[j]:
                memory_cur += graph[j].get_resource()
            if i in state_not_ending[j]:
                state_not_ending[j].remove(i)
        state_not_ending[i] = set(i for i in graph[i].get_children())
        if memory_cur > pick_of_memory:
            pick_of_memory = memory_cur
    return (pick_of_memory,)


def check_partial_order(permutation):
    state_was = set()
    for i in permutation:
        state_was.add(i)
        if (graph[i].get_parents() & state_was) != graph[i].get_parents():
            return False
    return True


def create_permutation(size):
    permutation_result = []
    set_ready_to_add = set()
    set_already_add = set()
    set_need_to_add = set(i for i in graph)
    while set_need_to_add:
        for i in set_need_to_add:
            if (graph[i].get_parents() & set_already_add) == graph[i].get_parents():
                set_ready_to_add.add(i)
        vertex_to_work = [i for i in set_ready_to_add][random.randint(0, len(set_ready_to_add)-1)]
        permutation_result.append(vertex_to_work)
        set_ready_to_add.remove(vertex_to_work)
        set_already_add.add(vertex_to_work)
        set_need_to_add.remove(vertex_to_work)
    return permutation_result


def tmp_evaluate(tmp_ind):
    return evaluate(tmp_ind)

def greedy_permutation(size):
    vertexes = list(graph.keys())
    random.shuffle(vertexes)
    permutation_result = []
    for v in cycle(vertexes):
        if v not in permutation_result:
            d = {}
            for count, i in enumerate(permutation_result):
                if (set(graph[v].get_parents()) & set(permutation_result[0:count])) == set(graph[v].get_parents()):
                    d[count] = tmp_evaluate(permutation_result[0:count]+[v]+permutation_result[count:])
            if (set(graph[v].get_parents()) & set(permutation_result)) == set(graph[v].get_parents()):
                d[len(permutation_result)] = tmp_evaluate(permutation_result + [v])
            if len(d) > 0:
                mincf = min(d.values())
                for i in d:
                    if d[i] == mincf:
                        permutation_result.insert(i, v)
                        break
        if len(permutation_result) == size:
            break
    return permutation_result

def my_population(populations, i):
    return populations[i]


def my_crossing_a(*ind):
    ind1, ind2 = ind
    breaking_point = random.randint(0, IND_SIZE-1)
    ind1_new = ind1[0:breaking_point]
    ind2_new = ind2[0:breaking_point]
    to_cycle2 = ind2[breaking_point:] + ind2[0:breaking_point]
    for i in cycle(to_cycle2):
        if i not in ind1_new:
            if (graph[i].get_parents() & set(ind1_new)) == graph[i].get_parents():
                ind1_new.append(i)
        if len(ind1_new) == IND_SIZE:
            break
    to_cycle1 = ind1[breaking_point:] + ind1[0:breaking_point]
    for i in cycle(to_cycle1):
        if i not in ind2_new:
            if (graph[i].get_parents() & set(ind2_new)) == graph[i].get_parents():
                ind2_new.append(i)
        if len(ind2_new) == IND_SIZE:
            break
    ind1.clear()
    for j, i in enumerate(ind1_new):
        ind1.insert(j, i)
    ind2.clear()
    for j, i in enumerate(ind2_new):
        ind2.insert(j, i)
    return ind1, ind2


def my_crossing_b(*ind):
    ind1, ind2 = ind
    breaking_point = random.randint(0, IND_SIZE-1)

    to_cycle1 = ind2[breaking_point:] + ind1[0:]
    to_cycle2 = ind1[breaking_point:] + ind2[0:]
    ind1_new = []
    ind2_new = []

    for i in cycle(to_cycle1):
        if i not in ind1_new:
            if (graph[i].get_parents() & set(ind1_new)) == graph[i].get_parents():
                ind1_new.append(i)
        if len(ind1_new) == IND_SIZE:
            break

    for i in cycle(to_cycle2):
        if i not in ind2_new:
            if (graph[i].get_parents() & set(ind2_new)) == graph[i].get_parents():
                ind2_new.append(i)
        if len(ind2_new) == IND_SIZE:
            break
    
    ind1.clear()
    for j, i in enumerate(ind1_new):
        ind1.insert(j, i)
    ind2.clear()
    for j, i in enumerate(ind2_new):
        ind2.insert(j, i)
    return ind1, ind2


def my_crossing_v(*ind):
    ind1, ind2 = ind
    breaking_pointl = random.randint(0, IND_SIZE - 1)
    breaking_pointr = random.randint(0, IND_SIZE - 1)
    if breaking_pointr < breaking_pointl:
        breaking_pointl, breaking_pointr = breaking_pointr, breaking_pointl

    to_cycle1 = ind1[:breaking_pointl] + ind2[breaking_pointl:breaking_pointr] + ind1[breaking_pointr:] + ind1[breaking_pointl:breaking_pointr]
    to_cycle2 = ind2[:breaking_pointl] + ind1[breaking_pointl:breaking_pointr] + ind2[breaking_pointr:] + ind2[breaking_pointl:breaking_pointr]
    ind1_new = []
    ind2_new = []

    for i in cycle(to_cycle1):
        if i not in ind1_new:
            if (graph[i].get_parents() & set(ind1_new)) == graph[i].get_parents():
                ind1_new.append(i)
        if len(ind1_new) == IND_SIZE:
            break

    for i in cycle(to_cycle2):
        if i not in ind2_new:
            if (graph[i].get_parents() & set(ind2_new)) == graph[i].get_parents():
                ind2_new.append(i)
        if len(ind2_new) == IND_SIZE:
            break

    ind1.clear()
    for j, i in enumerate(ind1_new):
        ind1.insert(j, i)
    ind2.clear()
    for j, i in enumerate(ind2_new):
        ind2.insert(j, i)

    return ind1, ind2


def my_crossing_g(ind1, ind2, indpb):
    breaking_point = random.randint(0, IND_SIZE - 1)

    to_cycle1 = []
    to_cycle2 = []
    after_1 = []
    after_2 = []
    for i in range(IND_SIZE):
        if random.random() < indpb:
            if ind2[i] not in to_cycle1:
                to_cycle1.append(ind2[i])
            after_1.append(ind1[i])
            if ind1[i] not in to_cycle2:
                to_cycle2.append(ind1[i])
            after_2.append(ind2[i])
        else:
            to_cycle1.append(ind1[i])
            to_cycle2.append(ind2[i])
    for i in after_1:
        if i not in to_cycle1:
            to_cycle1.append(i)
    for i in after_2:
        if i not in to_cycle2:
            to_cycle2.append(i)
    ind1_new = []
    ind2_new = []

    for i in cycle(to_cycle1):
        if i not in ind1_new:
            if (graph[i].get_parents() & set(ind1_new)) == graph[i].get_parents():
                ind1_new.append(i)
        if len(ind1_new) == IND_SIZE:
            break

    for i in cycle(to_cycle2):
        if i not in ind2_new:
            if (graph[i].get_parents() & set(ind2_new)) == graph[i].get_parents():
                ind2_new.append(i)
        if len(ind2_new) == IND_SIZE:
            break

    ind1.clear()
    for j, i in enumerate(ind1_new):
        ind1.insert(j, i)
    ind2.clear()
    for j, i in enumerate(ind2_new):
        ind2.insert(j, i)
    return ind1, ind2


def my_crossing_d(*ind):
    ind1, ind2 = ind
    breaking_pointl = random.randint(0, IND_SIZE - 1)
    breaking_pointr = random.randint(0, IND_SIZE - 1)
    if breaking_pointr < breaking_pointl:
        breaking_pointl, breaking_pointr = breaking_pointr, breaking_pointl

    to_cycle1 = list(ind1)
    to_cycle2 = list(ind2)

    holes1, holes2 = [True] * IND_SIZE, [True] * IND_SIZE
    for i in range(IND_SIZE):
        if i < breaking_pointl or i > breaking_pointr:
            holes1[to_cycle2[i]] = False
            holes2[to_cycle1[i]] = False
    temp1, temp2 = to_cycle1, to_cycle2
    k1, k2 = breaking_pointr + 1, breaking_pointr + 1
    for i in range(IND_SIZE):
        if not holes1[temp1[(i + breaking_pointr + 1) % IND_SIZE]]:
            to_cycle1[k1 % IND_SIZE] = temp1[(i + breaking_pointr + 1) % IND_SIZE]
            k1 += 1

        if not holes2[temp2[(i + breaking_pointr + 1) % IND_SIZE]]:
            to_cycle2[k2 % IND_SIZE] = temp2[(i + breaking_pointr + 1) % IND_SIZE]
            k2 += 1

    for i in range(breaking_pointl, breaking_pointr + 1):
        to_cycle1[i], to_cycle2[i] = to_cycle2[i], to_cycle1[i]

    ind1_new = []
    ind2_new = []

    for i in cycle(to_cycle1):
        if i not in ind1_new:
            if (graph[i].get_parents() & set(ind1_new)) == graph[i].get_parents():
                ind1_new.append(i)
        if len(ind1_new) == IND_SIZE:
            break

    for i in cycle(to_cycle2):
        if i not in ind2_new:
            if (graph[i].get_parents() & set(ind2_new)) == graph[i].get_parents():
                ind2_new.append(i)
        if len(ind2_new) == IND_SIZE:
            break

    ind1.clear()
    for j, i in enumerate(ind1_new):
        ind1.insert(j, i)
    ind2.clear()
    for j, i in enumerate(ind2_new):
        ind2.insert(j, i)
    return ind1, ind2


def my_mutate_exchange_a(*ind):
    ind = ind[0]
    for _ in range(len(ind)):
        breaking_point = random.randint(0, IND_SIZE - 1)
        mut_l = ind[breaking_point]
        vertex_after = [i for i in ind[breaking_point+1:]]
        vertex_before = set(i for i in ind[:breaking_point])
        to_mutate = []
        for i in vertex_after:
            if (graph[i].get_parents() & vertex_before) == graph[i].get_parents():
                to_mutate.append(i)
            else:
                break
        ind_new = [i for i in ind]
        if len(to_mutate):
            mut_r = random.choice(to_mutate)
            ind_new_ = []
            for i in ind:
                if i == mut_l:
                    ind_new_.append(mut_r)
                elif i == mut_r:
                    ind_new_.append(mut_l)
                else:
                    ind_new_.append(i)
        else:
            continue
        if check_partial_order(ind_new_):
            ind_new = ind_new_
            break
    ind.clear()
    for j, i in enumerate(ind_new):
        ind.insert(j, i)
    return (ind,)


def my_mutate_exchange_b(*ind):
    ind = ind[0]
    check_pos = [True for _ in range(len(ind))]
    ind_new = [i for i in ind]
    while any(check_pos):
        breaking_point = random.randint(0, IND_SIZE - 1)
        check_pos[breaking_point] = False
        mut_l = ind[breaking_point]
        vertex_after = [i for i in ind[breaking_point+1:]]
        vertex_before = set(i for i in ind[:breaking_point])
        to_mutate = []
        for i in vertex_after:
            if (graph[i].get_parents() & vertex_before) == graph[i].get_parents():
                to_mutate.append(i)
            else:
                break
        while len(to_mutate):
            mut_r = random.choice(to_mutate)
            to_mutate.remove(mut_r)
            ind_new_ = []
            for i in ind:
                if i == mut_l:
                    ind_new_.append(mut_r)
                elif i == mut_r:
                    ind_new_.append(mut_l)
                else:
                    ind_new_.append(i)
            if check_partial_order(ind_new_):
                ind_new = ind_new_
                break
        else:
            continue
    ind.clear()
    for j, i in enumerate(ind_new):
        ind.insert(j, i)
    return (ind,)


def my_mutate_exchange_v(*ind):
    ind = ind[0]
    check_pos = [True for _ in range(len(ind))]
    ind_new = [i for i in ind]
    while any(check_pos):
        breaking_point = random.randint(0, IND_SIZE - 1)
        check_pos[breaking_point] = False
        mut_l = ind[breaking_point]
        vertex_after = [i for i in ind[breaking_point + 1:]]
        vertex_before = [i for i in ind[:breaking_point]]
        to_mutate = []
        for i in vertex_after:
            if mut_l not in graph[i].get_parents():
                to_mutate.append(i)
            else:
                to_mutate.append(i)
                break
        for i in vertex_before[::-1]:
            if i not in graph[mut_l].get_parents():
                to_mutate.append(i)
            else:
                break
        if len(to_mutate) == 0:
            continue
        else:
            # не вставить на то же место
            mut_r = random.choice(to_mutate)
            ind_new = []
            for i in ind:
                if i != mut_l and i != mut_r:
                    ind_new.append(i)
                elif i == mut_r:
                    ind_new.append(mut_l)
                    ind_new.append(mut_r)
            break
    ind.clear()
    for j, i in enumerate(ind_new):
        ind.insert(j, i)
    return (ind,)


def my_mutate_exchange_v3(*ind):
    ind = ind[0]
    for _ in range(3):
        check_pos = [True for _ in range(len(ind))]
        ind_new = [i for i in ind]
        while any(check_pos):
            breaking_point = random.randint(0, IND_SIZE - 1)
            check_pos[breaking_point] = False
            mut_l = ind[breaking_point]
            vertex_after = [i for i in ind[breaking_point + 1:]]
            vertex_before = [i for i in ind[:breaking_point]]
            to_mutate = []
            for i in vertex_after:
                if mut_l not in graph[i].get_parents():
                    to_mutate.append(i)
                else:
                    to_mutate.append(i)
                    break
            for i in vertex_before[::-1]:
                if i not in graph[mut_l].get_parents():
                    to_mutate.append(i)
                else:
                    break
            if len(to_mutate) == 0:
                continue
            else:
                # не вставить на то же место
                mut_r = random.choice(to_mutate)
                ind_new = []
                for i in ind:
                    if i != mut_l and i != mut_r:
                        ind_new.append(i)
                    elif i == mut_r:
                        ind_new.append(mut_l)
                        ind_new.append(mut_r)
                break
    ind.clear()
    for j, i in enumerate(ind_new):
        ind.insert(j, i)
    return (ind,)


def varAnd(population, toolbox, cxpb, mutpb):
    offspring = [toolbox.clone(ind) for ind in population]
    for i in range(1, len(offspring), 2):
        if random.random() < cxpb:
            offspring[i - 1], offspring[i] = toolbox.mate(offspring[i - 1],
                                                          offspring[i])
            del offspring[i - 1].fitness.values, offspring[i].fitness.values

    for i in range(len(offspring)):
        if random.random() < mutpb:
            offspring[i], = toolbox.mutate(offspring[i])
            del offspring[i].fitness.values

    return offspring


def my_eaSimple(population, toolbox, cxpb, mutpb, ngen, stats=None, halloffame=None, verbose=__debug__):
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    invalid_ind = [ind for ind in population]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    fir_ar = []
    for ind, fit in zip(invalid_ind, fitnesses):
        fir_ar.append(fit)
        ind.fitness.values = fit
    min_fit_old = min(fir_ar)
    count_without_change = 0

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    if verbose:
        print(logbook.stream)

    gen = 0
    while count_without_change < NO_CHANGE_GEN:
        gen += 1
        offspring = toolbox.select(population, len(population))

        offspring = varAnd(offspring, toolbox, cxpb, mutpb)

        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        fir_ar = []
        for ind, fit in zip(invalid_ind, fitnesses):
            fir_ar.append(fit)
            ind.fitness.values = fit
        min_fit = min(fir_ar)
        if min_fit < min_fit_old:
            count_without_change = 0
            min_fit_old = min_fit
        else:
            count_without_change += 1

        if halloffame is not None:
            halloffame.update(offspring)

        population[:] = offspring

        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
        if verbose:
            print(logbook.stream)

    return population, logbook

def my_eaSimple_mut_up(population, toolbox, cxpb, mutpb, ngen, stats=None, halloffame=None, verbose=__debug__):
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    fir_ar = []
    for ind, fit in zip(invalid_ind, fitnesses):
        fir_ar.append(fit)
        ind.fitness.values = fit
    min_fit_old = min(fir_ar)
    count_without_change = 0
    flag_was_transform = 0

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    if verbose:
        print(logbook.stream)

    gen = 0
    while count_without_change < NO_CHANGE_GEN:
        gen += 1
        offspring = toolbox.select(population, len(population))

        offspring = varAnd(offspring, toolbox, cxpb, mutpb)

        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        fir_ar = []
        for ind, fit in zip(invalid_ind, fitnesses):
            fir_ar.append(fit)
            ind.fitness.values = fit
        min_fit = min(fir_ar)
        if min_fit < min_fit_old:
            count_without_change = 0
            min_fit_old = min_fit
        else:
            count_without_change += 1

        if halloffame is not None:
            halloffame.update(offspring)

        population[:] = offspring

        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
        if verbose:
            print(logbook.stream)
        if flag_was_transform == MUT_NON_STANDART:
            flag_was_transform += 1
            mutpb = P_MUTATION
        elif flag_was_transform > 0 and flag_was_transform < MUT_NON_STANDART:
            flag_was_transform += 1
        elif flag_was_transform == 0 and count_without_change == NO_CHANGE_GEN:
            count_without_change = 0
            flag_was_transform = 1
            mutpb = NEW_P_MUTATION

    return population, logbook


def migrate(populations, count_island):
    new_population = []
    for i in range(count_island):
        population_i = populations[i][0:MIGRATION_SIZE] + populations[(i+1)%count_island][MIGRATION_SIZE:]
        new_population.append(population_i)
    return new_population

def evolution(graph):
    random.seed()

    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMin)

    toolbox = base.Toolbox()
    toolbox.register("indices", greedy_permutation, IND_SIZE)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)

    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    population = toolbox.population(n=POPULATION_SIZE)

    toolbox.register("evaluate", evaluate)
    toolbox.register("select", tools.selTournament, tournsize=4)
    toolbox.register("mate", my_crossing_v)
    toolbox.register("mutate", my_mutate_exchange_v)

    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("min", min)

    halloffame = tools.HallOfFame(1,)

    population, logbook = my_eaSimple(population, toolbox, cxpb=P_CROSSOVER, mutpb=P_MUTATION, ngen=MAX_GENERATIONS,
                                      stats=stats, halloffame=halloffame, verbose=False)

    minFitnessValues = logbook.select("min")
    pokolenii = len(logbook) - 1

    best_evaluate = evaluate(halloffame[0])[0]
    best_ind = halloffame[0]  # перестановка-ответ
    return best_evaluate, best_ind, pokolenii


def evolution_island(graph):
    count_island = 4
    count_migration = 4
    random.seed()
    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMin)

    start_population = []
    toolbox = base.Toolbox()
    toolbox.register("indices", greedy_permutation, IND_SIZE)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    for i in range(count_island):
        population = toolbox.population(n=POPULATION_SIZE)
        start_population.append(population)

    result_evaluate = evaluate(population[0])
    result_best_ind = population[0]
    result_pokolenii = 0
    #result_minFitnessValues = 0

    for mig in range(count_migration):
        stop_populations = []
        best_evaluate_in_all = 0
        for i in range(count_island):
            random.seed()
            population = start_population[i]
            toolbox = base.Toolbox()
            toolbox.register("indices", greedy_permutation, IND_SIZE)
            toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)

            toolbox.register("population", my_population)
            population = toolbox.population(start_population, i)

            toolbox.register("evaluate", evaluate)
            toolbox.register("select", tools.selTournament, tournsize=4)
            toolbox.register("mate", my_crossing_v)
            toolbox.register("mutate", my_mutate_exchange_v)

            stats = tools.Statistics(lambda ind: ind.fitness.values)
            stats.register("min", min)

            halloffame = tools.HallOfFame(1, )

            population, logbook = my_eaSimple(population, toolbox, cxpb=P_CROSSOVER, mutpb=P_MUTATION, ngen=MAX_GENERATIONS,
                                              stats=stats, halloffame=halloffame, verbose=False)
            minFitnessValues = logbook.select("min")
            pokolenii = len(logbook) - 1
            best_evaluate = evaluate(halloffame[0])[0]
            best_ind = halloffame[0]
            if best_evaluate_in_all == 0:
                best_evaluate_in_all = best_evaluate
                result_evaluate = best_evaluate
                result_best_ind = best_ind
            elif best_evaluate < best_evaluate_in_all:
                best_evaluate_in_all = best_evaluate
                result_evaluate = best_evaluate
                result_best_ind = best_ind

            #print(mig, i, best_evaluate)
            stop_populations.append(population)
        start_population = migrate(stop_populations, count_island)
    return result_evaluate, result_best_ind


def evolution3(graph):
    random.seed()

    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMin)

    toolbox = base.Toolbox()
    toolbox.register("indices", create_permutation, IND_SIZE)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)

    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    population = toolbox.population(n=POPULATION_SIZE)

    toolbox.register("evaluate", evaluate)
    toolbox.register("select", tools.selTournament, tournsize=4)
    toolbox.register("mate", my_crossing_v)
    toolbox.register("mutate", my_mutate_exchange_v)

    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("min", min)

    halloffame = tools.HallOfFame(1,)

    population, logbook = my_eaSimple(population, toolbox, cxpb=P_CROSSOVER, mutpb=P_MUTATION, ngen=MAX_GENERATIONS,
                                      stats=stats, halloffame=halloffame, verbose=False)

    minFitnessValues = logbook.select("min")
    pokolenii = len(logbook) - 1

    best_evaluate = evaluate(halloffame[0])[0]
    best_ind = halloffame[0]  # перестановка-ответ

    for i in range(2):
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)
        population = toolbox.population(n=POPULATION_SIZE)

        toolbox.register("evaluate", evaluate)
        toolbox.register("select", tools.selTournament, tournsize=4)
        toolbox.register("mate", my_crossing_v)
        toolbox.register("mutate", my_mutate_exchange_v)

        stats = tools.Statistics(lambda ind: ind.fitness.values)
        stats.register("min", min)

        halloffame = tools.HallOfFame(1, )
        population, logbook = my_eaSimple(population, toolbox, cxpb=P_CROSSOVER, mutpb=P_MUTATION, ngen=MAX_GENERATIONS,
                                          stats=stats, halloffame=halloffame, verbose=False)
        if evaluate(halloffame[0])[0] < best_evaluate:
            minFitnessValues = logbook.select("min")
            pokolenii = len(logbook) - 1

            best_evaluate = evaluate(halloffame[0])[0]
            best_ind = halloffame[0]
    return best_evaluate, best_ind, pokolenii, minFitnessValues

def run_one_dag():
    number_of_graph = 143
    f_name = f"graphs/dag{number_of_graph}.txt"
    f_name = "/Users/macbookpro/Desktop/genetic_memory/single-proc-data/datasets/sausages/dagA5.txt"
    #f_name = "/Users/macbookpro/Desktop/genetic_memory/graphs_files/daglines.txt"
    global graph
    global IND_SIZE
    graph = create_graph(f_name)
    IND_SIZE = len(graph)
    cf, best_ind, p, gr = evolution(graph)
    print(f"лучший результат: {cf}, {check_partial_order(best_ind)}")

    #R59 : sao = 629 greedy  = 716 my1 = 762 my2 = 691

    """
    plt.plot(stat, color='red')
    plt.xlabel('Поколение')
    plt.ylabel('Лучшая цф в поколении')
    plt.show()
    """


def run_all_dags():
    dir_path = "/Users/macbookpro/Desktop/genetic_memory/triangle/graphs"
    s = "ABCDEFGHIKLMNOOPQR"
    for i in [40, 50, 60]:
        for f_name in os.listdir(dir_path):
            full_path = os.path.join(dir_path, f_name)
            number = full_path.split("triadag")[1]
            size = int(number.split("_")[0])
            if size == i:
                number = int(number.split(("_"))[1].split(".")[0])
                if number == 5 or number == 4:
                    print(size, number)
                    global graph
                    global IND_SIZE
                    graph = create_graph(full_path)
                    IND_SIZE = len(graph)
                    cf, best_ind, a, b = evolution(graph)
                    print(f"{cf}")



def run_study_stability_time():
    dir_path = "/Users/macbookpro/Desktop/genetic_memory/single-proc-data/datasets/sausages"
    s = "ABDEFG"
    n = 0
    d = {"A":0, "B":0, "C":2, "D":0, "E":0, "F":0, "G":0, "H":14, "I":18, "J":12, "K":6, "L":9}
    for i_s in d:
        full_path = f"{dir_path}/dag{i_s}{d[i_s]}.txt"
        list_cf = []
        list_iter = []
        list_time = []
        global graph
        global IND_SIZE
        graph = create_graph(full_path)
        IND_SIZE = len(graph)
        print(i_s, n)
        for i in range(7):
            print(i)
            start_time = time.time()
            cf, best_ind, poc = evolution(graph)
            end_time = time.time()
            t = end_time - start_time
            print(t)
            list_time.append(t)
            #list_cf.append(cf)
            #list_iter.append(poc)
        print(max(list_time), min(list_time), sum(list_time)/len(list_time))
        print(list_time)
        """
        with open("/Users/macbookpro/Desktop/genetic_memory/results/triangle_all_0_1.txt", "a") as f:
            f.write(f"{size} {count_in_size}\n")
            f.write(f"{max(list_cf)}, {min(list_cf)}, {sum(list_cf)/len(list_cf)}\n")
            s = ",".join(map(str, list_cf))
            f.write(f"{s}\n")
            f.write(f"{max(list_iter)}, {min(list_iter)}, {sum(list_iter) / len(list_iter)}\n")
            s = ",".join(map(str, list_iter))
            f.write(f"{s}\n")
        """

def f():
    dir_path = "/Users/macbookpro/Desktop/genetic_memory/single-proc-data/datasets/sausages"
    s = "ABCDEFGHIJKLMNOPQR"
    for i in s:
        for f_name in os.listdir(dir_path):
            if f_name[3] == i:
                full_path = os.path.join(dir_path, f_name)
                global graph
                graph = create_graph(full_path)
                sum = 0
                for v in graph:
                    sum += len(graph[v].get_children())
                print(i, f_name[4:], sum)

"""
def run_study_stability():
    dir_path = "/Users/macbookpro/Desktop/genetic_memory/single-proc-data/datasets/sausages"
    d = {"B":[35,15,4,3,17], "D":[5,28,21,0,4], "F":[22,0,31,44,3], "H":[36,61,31,43,52], "I":[36,41,44,25,46],
         "J":[28,52,60,33,18], "K":[26,14,10,29,15], "M":[15,11,37,8,12], "O":[16,26,15,62,13], "Q":[51,16,41,26,38]
    for i in d:
        for j in d[i]:
            full_path = f"{dir_path}/dag{i}{j}.txt"
            list_cf = []
            list_iter = []
            list_time = []
            global graph
            global IND_SIZE
            graph = create_graph(full_path)
            IND_SIZE = len(graph)
            print(i, j)
            for _ in range(5):
                start_time = time.time()
                cf, best_ind, poc = evolution(graph)
                end_time = time.time()
                t = end_time - start_time
                print(cf)
                list_time.append(t)
                list_cf.append(cf)
                list_iter.append(poc)
            print(max(list_cf), min(list_cf), sum(list_cf)/len(list_cf))
            print(list_cf)
            print(list_time)
"""

def evolutiona(graph):
    random.seed()

    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMin)

    toolbox = base.Toolbox()
    toolbox.register("indices", create_permutation, IND_SIZE)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)

    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    population = toolbox.population(n=POPULATION_SIZE)

    toolbox.register("evaluate", evaluate)
    toolbox.register("select", tools.selTournament, tournsize=4)
    toolbox.register("mate", my_crossing_v)
    toolbox.register("mutate", my_mutate_exchange_v)

    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("min", min)

    halloffame = tools.HallOfFame(1, )

    population, logbook = my_eaSimple(population, toolbox, cxpb=P_CROSSOVER, mutpb=P_MUTATION, ngen=MAX_GENERATIONS,
                                      stats=stats, halloffame=halloffame, verbose=False)

    minFitnessValues = logbook.select("min")
    pokolenii = len(logbook) - 1

    best_evaluate = evaluate(halloffame[0])[0]
    best_ind = halloffame[0]  # перестановка-ответ
    return minFitnessValues, best_evaluate

def evolutionb(graph):
    random.seed()

    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMin)

    toolbox = base.Toolbox()
    toolbox.register("indices", create_permutation, IND_SIZE)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)

    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    population = toolbox.population(n=POPULATION_SIZE)

    toolbox.register("evaluate", evaluate)
    toolbox.register("select", tools.selTournament, tournsize=4)
    toolbox.register("mate", my_crossing_v)
    toolbox.register("mutate", my_mutate_exchange_v)

    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("min", min)

    halloffame = tools.HallOfFame(1, )

    population, logbook = eaSimple(population, toolbox, cxpb=P_CROSSOVER, mutpb=P_MUTATION, ngen=MAX_GENERATIONS,
                                      stats=stats, halloffame=halloffame, verbose=False)

    minFitnessValues = logbook.select("min")
    pokolenii = len(logbook) - 1

    best_evaluate = evaluate(halloffame[0])[0]
    best_ind = halloffame[0]  # перестановка-ответ
    return minFitnessValues, best_evaluate

def evolutionv(graph):
    random.seed()

    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMin)

    toolbox = base.Toolbox()
    toolbox.register("indices", create_permutation, IND_SIZE)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)

    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    population = toolbox.population(n=POPULATION_SIZE)

    toolbox.register("evaluate", evaluate)
    toolbox.register("select", tools.selTournament, tournsize=4)
    toolbox.register("mate", my_crossing_v)
    toolbox.register("mutate", my_mutate_exchange_v)

    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("min", min)

    halloffame = tools.HallOfFame(1, )

    population, logbook = my_eaSimple_mut_up(population, toolbox, cxpb=P_CROSSOVER, mutpb=P_MUTATION, ngen=MAX_GENERATIONS,
                                      stats=stats, halloffame=halloffame, verbose=False)

    minFitnessValues = logbook.select("min")
    pokolenii = len(logbook) - 1

    best_evaluate = evaluate(halloffame[0])[0]
    best_ind = halloffame[0]  # перестановка-ответ
    return minFitnessValues, best_evaluate

def evolutiong(graph):
    random.seed()

    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMin)

    toolbox = base.Toolbox()
    toolbox.register("indices", create_permutation, IND_SIZE)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)

    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    population = toolbox.population(n=POPULATION_SIZE)

    toolbox.register("evaluate", evaluate)
    toolbox.register("select", tools.selTournament, tournsize=4)
    toolbox.register("mate", my_crossing_g, indpb=0.35)
    toolbox.register("mutate", my_mutate_exchange_v)

    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("min", min)

    halloffame = tools.HallOfFame(1, )

    population, logbook = eaSimple(population, toolbox, cxpb=P_CROSSOVER, mutpb=P_MUTATION, ngen=MAX_GENERATIONS,
                                      stats=stats, halloffame=halloffame, verbose=False)

    minFitnessValues = logbook.select("min")
    pokolenii = len(logbook) - 1

    best_evaluate = evaluate(halloffame[0])[0]
    best_ind = halloffame[0]  # перестановка-ответ
    return minFitnessValues, best_evaluate

def simple_pict():
    dir_path = "/Users/macbookpro/Desktop/genetic_memory/single-proc-data/datasets/sausages"
    full_path = f"{dir_path}/dagE0.txt"
    global graph
    global IND_SIZE
    graph = create_graph(full_path)
    IND_SIZE = len(graph)

    stata, cfa = evolutiona(graph)
    print('a ', cfa)
    statb, cfb = evolutionb(graph)
    print('b ', cfb)
    global NO_CHANGE_GEN
    NO_CHANGE_GEN = NO_CHANGE_GEN / 2
    #statv, cfv = evolutionv(graph)
    #print('v ', cfv)

    plt.plot(statb, color='green', label='фиксированное число поколений')
    plt.plot(stata, color='red', label = 'фиксированное число поколений без улучшения целевой функции')
    #plt.plot(statv, color='blue', label = 'фчп с цф - встряска - фчп с цф')
    plt.legend(fontsize=8)
    plt.show()


#run_one_dag()
#run_all_dags()
#run_study_stability()
#f()
simple_pict()